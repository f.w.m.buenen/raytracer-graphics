﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Template
{
    abstract class Primitive
    {
        private Vector3 color;
        private bool isChecker;
        private float reflectance;
        public Vector3 Color { get { return color; } }
        public bool IsChecker { get { return isChecker; } }
        public float Reflectance { get { return reflectance; } }
        public Vector3 SpecularColor { get; }
        public float Specularity { get; }

        protected Primitive(Vector3 col, bool isChecker = false, float reflectance = 0f)
        {
            color = col;
            this.isChecker = isChecker;
            this.reflectance = MathHelper.Clamp(reflectance, 0, 100);
        }
        
        //constructor for specularity option
        protected Primitive(Vector3 col, float specularity, Vector3 specularColor, bool isChecker = false, float reflectance = 0f)
        {
            color = col;
            this.isChecker = isChecker;
            this.reflectance = MathHelper.Clamp(reflectance, 0, 100);
            SpecularColor = specularColor;
            Specularity = specularity;
        }

        //returns a color on a checkerboard, given world (x,y) coordinates on the surface
        public Vector3 CalculateChecker(float x, float y, float scale)
        {
            if (x < 0) x -= scale;
            if (y < 0) y -= scale;

            if ((int)(x / scale) % 2 != 0 & (int)(y / scale) % 2 != 0)
                return Vector3.Zero;
            if ((int)(x / scale) % 2 == 0 & (int)(y / scale) % 2 != 0)
                return Vector3.One;
            if ((int)(x / scale) % 2 != 0 & (int)(y / scale) % 2 == 0)
                return Vector3.One;

            return Vector3.Zero;
        }

        //calculates intersection with ray
        protected void Intersection()
        {

        }
    }
}
