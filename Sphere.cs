﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Template
{
    class Sphere : Primitive
    {
        float radius;
        private Vector3 position;

        public float Radius { get { return radius; } }
        public Vector3 Position { get { return position; } }

        public Sphere(Vector3 pos, Vector3 col, float rad, float reflectance = 0f) : base(col, false, reflectance)
        {
            radius = rad;
            position = pos;
        }

        //constructor for specularity option
        public Sphere(Vector3 pos, Vector3 col, float rad, float specularity, Vector3 specularColor, float reflectance = 0f) : base(col, specularity, specularColor, false, reflectance)
        {
            radius = rad;
            position = pos;
        }

        //returns intersection information of a ray with a sphere. returns zero if no intersection was found.
        public Intersection IntersectSphere(Ray ray)
        {
            Vector3 c = position - ray.origin;
            float t = Vector3.Dot(c, ray.direction);
            ray.t = t;
            Vector3 q = c - t * ray.direction;
            float p2 = Vector3.Dot(q, q);
            if (p2 > (radius * radius)) return null;

            t -= (float)Math.Sqrt((radius * radius) - p2);
            if ((t < ray.t) && (t > 0)) {
                Vector3 n = Vector3.Normalize((ray.direction * t) - c);

                return new Intersection(t, this, n);
            }

            return null;
        }
    }
}
