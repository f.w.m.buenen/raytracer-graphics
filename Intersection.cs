﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Template
{
    //contains information about intersection between ray and primitive
    class Intersection
    {
        public float Distance { get; set; }
        public Primitive NearestPrim { get; set; }
        public Vector3 IntersectNormal { get; set; }

        public Intersection(float dist, Primitive nearPrim, Vector3 intNorm)
        {
            Distance = dist;
            NearestPrim = nearPrim;
            IntersectNormal = intNorm;
        }
        
    }
}
