﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Template
{
    class SpotLight : Light
    {
        public float ConeAngle { get; }
        public Vector3 Direction { get; }

        float halfAngle;

        public SpotLight(Vector3 loc, Vector3 intens, Vector3 dir, float angle) : base(loc, intens)
        {
            ConeAngle = angle;
            Direction = Vector3.Normalize(dir);
            halfAngle = ConeAngle / 2;
        }

        //checks if object is lit by spotlight and, if so, returns theta
        public override float CalculateTheta(Intersection i, Vector3 L)
        {
            float theta = Vector3.Dot(i.IntersectNormal, L);
            if (theta <= 0)
                return 0;

            L = -1 * Vector3.Normalize(L);
            float intersectAngle = (float)Math.Acos(Vector3.Dot(L, Direction));
            intersectAngle = MathHelper.RadiansToDegrees(intersectAngle);

            if (intersectAngle > halfAngle)
            {
                return 0;
            }

            return theta;
        }
    }
}
