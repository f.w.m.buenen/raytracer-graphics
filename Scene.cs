﻿using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace Template
{
    class Scene
    {
        public List<Primitive> primitives = new List<Primitive>();
        public List<Light> lights = new List<Light>();

        public Scene()
        {
            //add primitives to scene
            primitives.Add(new Plane(new Vector3(0, 1f, 0), 1.5f, new Vector3(1, 1, 1), 50, new Vector3(.3f, .3f, .3f), true, 40));
            primitives.Add(new Sphere(new Vector3(-3.5f, 0.0f, 6.5f), new Vector3(1, 0, 1), 1.5f, 50, new Vector3(.3f, .3f, .3f), 40));
            primitives.Add(new Sphere(new Vector3(0.0f, 0.0f, 7.5f), new Vector3(.2f, .9f, 1f), 1.5f, 1000, new Vector3(.5f, .5f, .5f), 90));
            primitives.Add(new Sphere(new Vector3(3.5f, 0.0f, 6.5f),  new Vector3(1, 1, .2f), 1.5f, 50, new Vector3(.3f, .3f, .3f), 40));

            //add lights to scene
            lights.Add(new Light(new Vector3(0, 6, 7.5f), new Vector3(2f, 2f, 2f)));
            lights.Add(new Light(new Vector3(0, 1.6f, 2.5f), new Vector3(2f, 1f, 1f)));
            lights.Add(new SpotLight(new Vector3(-2.5f, 10, 1.5f), new Vector3(4f, 4f, 4f), new Vector3(-3.5f, -10f, 6.5f), 45));
            lights.Add(new SpotLight(new Vector3(0, 10, 1.5f), new Vector3(4f, 4f, 4f), new Vector3(0.0f, -10f, 7.5f), 25));
            lights.Add(new SpotLight(new Vector3(2.5f, 10, 1.5f), new Vector3(4f, 4f, 4f), new Vector3(3.5f, -10f, 6.5f), 45));
        }

        //Loops over primitives and returns closest intersection
        public Intersection Intersect(Ray ray)
        {
            Intersection nearestPrim = null;

            //calculate sphere intersections
            foreach (Sphere s in primitives.OfType<Sphere>())
            {
                Intersection i = s.IntersectSphere(ray);
                if (i != null)
                {
                    //check if primitve that was hit by ray is closer then current 'nearest primitive'
                    if (nearestPrim == null || i.Distance < nearestPrim.Distance)
                        nearestPrim = i;
                }
            }

            //calculate plane intersections
            foreach (Plane pl in primitives.OfType<Plane>())
            {
                Intersection i = pl.IntersectPlane(ray);
                if (i != null)
                {
                    //check if primitve that was hit by ray is closer then current 'nearest primitive'
                    if (nearestPrim == null || i.Distance < nearestPrim.Distance)
                        nearestPrim = i;
                }
            }

            return nearestPrim;
        }
    }
}
