﻿using System;
using OpenTK;
using OpenTK.Input;
using System.Collections.Generic;
using System.Linq;

namespace Template
{
    //describes a ray
    public struct Ray
    {
        public Vector3 origin;
        public Vector3 direction; //normalized
        public float t; //distance
    }

    class RayTracer
    {
        Scene scene;
        Camera camera;
        public Surface screen, debugScreen;
        public static Vector2 screenSize;
        float xRange, offsetX, yRange, offsetY, zoom, rayMaxLength, 
            camOffsetDirX, camOffsetDirY, camOffsetPosX, camOffsetPosZ, camSpeed, gammaFactor;
        Vector3 p0, p1, p2, r, u, screenCenter;
        int[] colorBuffer;
        float[] HDRColorBuffer;
        int recursionMax, recursionCounter, pixelStep, debugRayModulo;
        List<int> debugPrimeRays, debugVertexNormals, debugShadowRays, debugSecRays;
        bool keyPressed, renderReflection, renderSpecular;

        //initialize all member vars
        public void Init()
        {
            xRange = 5;
            yRange = 5;
            zoom = 1;
            offsetX = 0;
            offsetY = 4.5f;

            camOffsetDirX = 0;
            camOffsetDirY = 0;
            camOffsetPosX = 0;
            camOffsetPosZ = 0;
            camSpeed = 0.1f;

            rayMaxLength = 25f - 0.00006f;

            screenSize = new Vector2(screen.width, screen.height);
            scene = new Scene();
            camera = new Camera(new Vector3(0, 0, 0), new Vector3(0, 0, 1f), 80f);
            gammaFactor = 1.0f / camera.Gamma;

            CalculateScreenCoordinates();

            recursionMax = 8;
            recursionCounter = 0;

            pixelStep = 1; //every n pixels are drawn

            colorBuffer = new int[screen.width * screen.height];
            HDRColorBuffer = new float[1000 * 1000 * 3];
            ReadSkydome();
            
            //create lists for debug information
            debugPrimeRays = new List<int>();
            debugVertexNormals = new List<int>();
            debugShadowRays = new List<int>();
            debugSecRays = new List<int>();

            int debugRayAmount = 5; //amount of primary rays drawn to debug screen
            debugRayModulo = (int)screen.width / debugRayAmount;

            keyPressed = false;
            renderReflection = true;
            renderSpecular = true;
            DrawScreen();
            DrawDebug();
        }

        public void Render()
        {
            if (keyPressed)
            {
                DrawScreen();
                return;
            }

            //if there is no user input and pixelstep is still on 5, reset pixel step to 1 and draw screen
            if (!keyPressed & pixelStep == 5)
            {
                pixelStep = 1;
                renderReflection = true;
                renderSpecular = true;
                DrawScreen();
                DrawDebug();
            }
            
        }

        //changes pixel array of screen
        private void DrawScreen()
        {
            screen.Clear(0);
            Array.Clear(colorBuffer, 0, colorBuffer.Length);
            CalculateIntersections();

            for (int i = 0; i<colorBuffer.Length; i++)
            {
                screen.pixels[i] = colorBuffer[i];
            }
        }

        public void RenderDebug()
        {
            //update debug screen online when there was user input
            if (keyPressed)
            {
                DrawDebug();
                keyPressed = false;
            }
        }

        //calls methods that draw debug screen
        private void DrawDebug()
        {
            debugScreen.Clear(0);
            DrawDebugPrimitives();
            DrawDebugMisc();
        }

        private void DrawDebugPrimitives()
        {
            //vars used for drawing spheres
            int nCirclePoints = 100;
            float aStep = (float)(2 * Math.PI) / nCirclePoints;
            float a = 0;

            foreach (Primitive p in scene.primitives)
            {
                if (p.GetType() == typeof(Sphere))
                {
                    Sphere s = p as Sphere;
                    float x, y, xs, ys;

                    //draw a line n times making up the circle
                    for (int i = 0; i < nCirclePoints; i++)
                    {
                        a = aStep * i;

                        x = s.Position.X + s.Radius * (float)Math.Cos(a);
                        y = s.Position.Z + s.Radius * (float)Math.Sin(a);

                        xs = s.Position.X + s.Radius * (float)Math.Cos(a + aStep);
                        ys = s.Position.Z + s.Radius * (float)Math.Sin(a + aStep);

                        debugScreen.Line(TX(x), TY(y), TX(xs), TY(ys), CreateColor(s.Color.X, s.Color.Y, s.Color.Z));
                    }
                }

            }

            a = 0;
            aStep = (float)(Math.PI) / 4;
            
            //draw debug light
            foreach (Light l in scene.lights)
            {
                float x, y, xs, ys;

                for (int i = 0; i <= 4; i++)
                {
                    a = aStep * i;
                    x = l.Location.X + .3f * (float)(Math.Cos(a));
                    y = l.Location.Z + .3f * (float)(Math.Sin(a));
                    xs = l.Location.X - .3f * (float)(Math.Cos(a));
                    ys = l.Location.Z - .3f * (float)(Math.Sin(a));

                    debugScreen.Line(TX(x), TY(y), TX(xs), TY(ys), CreateColor(l.Intensity.X, l.Intensity.Y, l.Intensity.Z));
                }
            }
        }

        private void DrawDebugMisc()
        {
            int screenX = TX(camera.Position.X);
            int screenY = TY(camera.Position.Z);
            //draw camera as yellow bar
            debugScreen.Bar(screenX-3 + screen.width, screenY - 3, screenX + 3 + screen.width, screenY + 3, 0xffff00);

            //calculate debug screen coordinates using normalized camera direction, disregarding its y-component
            Vector3 screenDir = Vector3.Normalize(new Vector3(camera.Direction.X, 0, camera.Direction.Z));
            float screenX0 = camera.Position.X + screenDir.Z + screenDir.X * camera.ScreenDist;
            float screenY0 = camera.Position.Z + -1 * screenDir.X + screenDir.Z * camera.ScreenDist;
            float screenX1 = camera.Position.X + -1 * screenDir.Z + screenDir.X * camera.ScreenDist;
            float screenY1 = camera.Position.Z + screenDir.X + screenDir.Z * camera.ScreenDist;

            //draw screen view as white line
            debugScreen.Line(TX(screenX0), TY(screenY0), TX(screenX1), TY(screenY1), 0xffffff);

            //draw primary rays
            for (int i = 0; i < debugPrimeRays.Count; i += 2)
            {
                debugScreen.Line(TX(camera.Position.X), TY(camera.Position.Z), debugPrimeRays[i], debugPrimeRays[i + 1], 0xffff00);
            }
            
            //draw vertex normals
            for (int i = 0; i < debugVertexNormals.Count; i += 4)
            {
                debugScreen.Line(debugVertexNormals[i], debugVertexNormals[i + 1], debugVertexNormals[i + 2], debugVertexNormals[i + 3], 0xffffff);
            }
            
            //draw shadow rays
            for (int i = 0; i < debugShadowRays.Count; i += 4)
            {
                debugScreen.Line(debugShadowRays[i], debugShadowRays[i + 1], debugShadowRays[i + 2], debugShadowRays[i + 3], 0x666666);
            }

            //draw secondary rays
            for (int i = 0; i < debugSecRays.Count; i += 4)
            {
                debugScreen.Line(debugSecRays[i], debugSecRays[i + 1], debugSecRays[i + 2], debugSecRays[i + 3], 0xffbb00);
            }

            //clear debug information
            debugPrimeRays.Clear();
            debugVertexNormals.Clear();
            debugShadowRays.Clear();
            debugSecRays.Clear();
        }

        //calculates screen coordinates as world coordinates based on the current camera position and direction
        private void CalculateScreenCoordinates()
        {
            screenCenter = camera.Position + camera.ScreenDist * camera.Direction; //center of screen plane

            r = Vector3.Normalize(Vector3.Cross(camera.Direction, new Vector3(0, 1, 0))); //vector that points right 
            u = Vector3.Cross(r, camera.Direction); //vector that points upward from screen
            p0 = screenCenter + r + u;
            p1 = screenCenter - r + u;
            p2 = screenCenter + r - u;
        }

        //calculates intersections between objects and primary rays
        public void CalculateIntersections()
        {
            float u, v;  //screen-coordinates in world space
            Ray ray = new Ray();
            ray.origin = camera.Position;

            for (int y = 0; y < screen.height; y += pixelStep)
            {
                for (int x = 0; x < screen.width; x += pixelStep)
                {
                    ray.t = rayMaxLength;
                    ray.origin = camera.Position;
                    u = (float)x / (float)screen.width;
                    v = (float)y / (float)screen.height;

                    Vector3 p = p0 + u * (p1 - p0) + v * (p2 - p0);  //point on screen
                    Vector3 primeRayDir = Vector3.Normalize(p - camera.Position);
                    ray.direction = primeRayDir; //ray direction
                    Intersection nearestPrim = scene.Intersect(ray); //primtive which is hit first by ray. returns null if not hit by ray

                    //if ray hit a primitive, set the ray length to the distance of the nearest primitive
                    if (nearestPrim != null)
                    {
                        ray.t = MathHelper.Clamp(nearestPrim.Distance, 0f, rayMaxLength);
                        CalculateDiffuse(nearestPrim, ray, x, y);

                        ray.direction = primeRayDir; //reset ray direction to original primary direction
                    }
                    else
                    {
                        Vector3 c = SkydomePixelColor(ray);
                        colorBuffer[x + y * screen.height] = CreateColor(c.X, c.Y, c.Z); //if no ray intersection -> set pixel color to black
                    }

                    Vector3 rayCoordinate = ray.direction * ray.t;

                    //add rays to debug screen if ray is shot through middle of screen
                    if (y == screen.height / 2 & x % debugRayModulo == 0)
                    {
                        if (nearestPrim != null)
                        {
                            //debug normal: normalized intersection normal where y-component is disregarded
                            Vector3 debugNormal = Vector3.Normalize(new Vector3(nearestPrim.IntersectNormal.X, 0, nearestPrim.IntersectNormal.Z));
                            
                            //add vertex normals to list for debugging
                            debugVertexNormals.Add(TX(rayCoordinate.X + camera.Position.X));
                            debugVertexNormals.Add(TY(rayCoordinate.Z + camera.Position.Z));
                            debugVertexNormals.Add(TX(rayCoordinate.X + camera.Position.X + .6f * debugNormal.X));
                            debugVertexNormals.Add(TY(rayCoordinate.Z + camera.Position.Z + .6f * debugNormal.Z));
                        }

                        debugPrimeRays.Add(TX(rayCoordinate.X + camera.Position.X));
                        debugPrimeRays.Add(TY(rayCoordinate.Z + camera.Position.Z));
                    }
                }
            }
        }

        //if secondary ray finds intersection, return color of intersected object, otherwise return black
        private Vector3 TraceSecondaryRay(Ray ray, Intersection i, Vector3 D, int x, int y)
        {
            //stop recursion if max recursion depth is reached
            if (recursionCounter == recursionMax)
            {
                recursionCounter = 0;
                return Vector3.One;
            }
            
            recursionCounter++;

            //initialize new ray values for checking secondary intersection
            ray.origin = ray.origin + ray.direction * i.Distance;
            ray.direction = D;
            ray.origin += ray.direction * 0.0003f;
            Intersection secInter = scene.Intersect(ray); //secondary ray intersection

            bool drawDebug = y == screen.height / 2 & x % debugRayModulo == 0;

            if (drawDebug)
            {
                debugSecRays.Add(TX(ray.origin.X));
                debugSecRays.Add(TY(ray.origin.Z));
            }

            if (secInter != null)
            {
                Vector3 intersectionPoint = ray.origin + ray.direction * secInter.Distance;
                if (drawDebug)
                {
                    debugSecRays.Add(TX(intersectionPoint.X));
                    debugSecRays.Add(TY(intersectionPoint.Z));
                }

                //if intersected object is reflective, recursively call TraceSecondaryRay
                if (secInter.NearestPrim.Reflectance != 0)
                {
                    float r = 0.01f * secInter.NearestPrim.Reflectance;
                    Vector3 direction = ReflectedDirection(ray, i);

                    //if object has checker pattern (floor), call mathod that calculates the right color (black or white)
                    if (secInter.NearestPrim.IsChecker)
                    {
                        return (1-r) * secInter.NearestPrim.CalculateChecker(intersectionPoint.X, intersectionPoint.Z, 0.3f) + r * TraceSecondaryRay(ray, secInter, direction, x, y);
                    }

                    return (1-r) * secInter.NearestPrim.Color + r * TraceSecondaryRay(ray, secInter, direction, x, y);
                }

                if (secInter.NearestPrim.IsChecker)
                {
                    recursionCounter = 0;
                    return secInter.NearestPrim.CalculateChecker(intersectionPoint.X, intersectionPoint.Z, 0.3f);
                }

                recursionCounter = 0;
                return secInter.NearestPrim.Color;
            }

            if (drawDebug)
            {
                Vector3 secRayCoord = ray.origin + ray.direction * ray.t;
                debugSecRays.Add(TX(secRayCoord.X));
                debugSecRays.Add(TY(secRayCoord.Z));
            }

            recursionCounter = 0; //no recursive calls made, reset counter to zero
            return SkydomePixelColor(ray);
        }

        //returns intersection object if shadow ray hit an object in the scene
        private bool ShadowRayIntersect(Ray ray, Light l, int x, int y)
        {
            //define new ray location and direction
            ray.origin = ray.direction * ray.t + camera.Position;
            ray.direction = Vector3.Normalize(l.Location - ray.origin);
            ray.origin += ray.direction * 0.0003f;

            Vector3 yCanceled = new Vector3(1, 0, 1);
            Vector3 debugDir = Vector3.Normalize(ray.direction * yCanceled);
            bool drawDebug = y == screen.height / 2 & x % debugRayModulo == 0;

            //check if shadow ray intersects with any sphere in the scene
            foreach (Sphere s in scene.primitives.OfType<Sphere>())
            {
                Intersection i = s.IntersectSphere(ray);

                if (i != null)
                {
                    if (drawDebug)
                    {
                        debugShadowRays.Add(TX(ray.origin.X));
                        debugShadowRays.Add(TY(ray.origin.Z));
                        debugShadowRays.Add(TX(ray.origin.X + debugDir.X * i.Distance));
                        debugShadowRays.Add(TY(ray.origin.Z + debugDir.Z * i.Distance));
                    }

                    return true;
                }
            }

            //check if shadow ray intersects with any plane in the scene
            foreach (Plane p in scene.primitives.OfType<Plane>())
            {
                Intersection i = p.IntersectPlane(ray);
                if (i != null)
                {
                    if (drawDebug)
                    {
                        debugShadowRays.Add(TX(ray.origin.X));
                        debugShadowRays.Add(TY(ray.origin.Z));
                        debugShadowRays.Add(TX(ray.origin.X + debugDir.X * i.Distance));
                        debugShadowRays.Add(TY(ray.origin.Z + debugDir.Z * i.Distance));
                    }

                    return true;
                }
            }

            ray.t = (l.Location * yCanceled - ray.origin * yCanceled).Length;
            if (drawDebug)
            {
                debugShadowRays.Add(TX(ray.origin.X));
                debugShadowRays.Add(TY(ray.origin.Z));
                debugShadowRays.Add(TX(ray.origin.X + debugDir.X * ray.t));
                debugShadowRays.Add(TY(ray.origin.Z + debugDir.Z * ray.t));
            }

            return false;
        }

        //sends per pixel diffuse color to color buffer
        private void CalculateDiffuse(Intersection i, Ray ray, int x, int y)
        {
            Vector3 matColor;

            if (i.NearestPrim.Reflectance != 0 && renderReflection)
            {
                float r = 0.01f * i.NearestPrim.Reflectance;
                Vector3 direction = ReflectedDirection(ray, i);

                if (i.NearestPrim.IsChecker)
                {
                    Vector3 point = ray.origin + ray.direction * ray.t;
                    matColor = (1-r) * i.NearestPrim.CalculateChecker(point.X, point.Z, 0.3f) + r * TraceSecondaryRay(ray, i, direction, x, y);
                }
                else
                {
                    matColor = (1-r) * i.NearestPrim.Color + r * TraceSecondaryRay(ray, i, direction, x, y);
                }

            }
            else if (i.NearestPrim.IsChecker)
            {
                Vector3 point = ray.origin + ray.direction * ray.t;
                matColor = i.NearestPrim.CalculateChecker(point.X, point.Z, 0.3f);
            }
            else
                matColor = i.NearestPrim.Color;

            Vector3 color = Vector3.Zero;

            //loop through all lights in the scene
            foreach (Light l in scene.lights)
            {
                Vector3 L = l.Location - (camera.Position + i.Distance * ray.direction);
                float attenuation = 1f / (L.Length * L.Length);
                float theta = l.CalculateTheta(i, L);
                Vector3 specularAddition;
                if (theta > 0 && renderSpecular)
                {
                    //calculate specular value
                    Vector3 V = camera.Position - (i.Distance * ray.direction);
                    Vector3 H = Vector3.Normalize(V + L);
                    float specDot = Math.Max(0, Vector3.Dot(i.IntersectNormal, H));
                    specularAddition = i.NearestPrim.SpecularColor * l.Intensity * (float)Math.Pow(specDot, i.NearestPrim.Specularity);
                }
                else
                    specularAddition = Vector3.Zero;
                

                if (!ShadowRayIntersect(ray, l, x, y))
                    color += l.Intensity * theta * matColor * attenuation + specularAddition;
            }

            //asign calculated color values to colorbuffer
            colorBuffer[x + y * screen.height] = CreateColor(color.X, color.Y, color.Z);
        }

        //returns the reflection direction, given a ray and intersection
        private Vector3 ReflectedDirection(Ray ray, Intersection i)
        {
            Vector3 L = (ray.origin + ray.direction * i.Distance) - ray.origin;
            Vector3 R = L - 2 * (Vector3.Dot(L, i.IntersectNormal)) * i.IntersectNormal;
            return Vector3.Normalize(R);
        }

        //create int color from HDR RGB values
        private int CreateColor(float red, float green, float blue)
        {
            //apply Reinhard tone mapping algorithm for HDR lighting
            float mappedRed = red / (red + 1.0f);
            float mappedGreen = green / (green + 1.0f);
            float mappedBlue = blue / (blue + 1.0f);

            //apply gamma factor
            mappedRed = (float)Math.Pow(mappedRed, gammaFactor);
            mappedGreen = (float)Math.Pow(mappedGreen, gammaFactor);
            mappedBlue = (float)Math.Pow(mappedBlue, gammaFactor);

            return ((int)(255 * mappedRed) << 16) + ((int)(255 * mappedGreen) << 8) + (int)(mappedBlue * 255);
        }

        //returns Vector3 color at a certain direction of the given ray
        private Vector3 SkydomePixelColor(Ray ray)
        {
            float r = (float)(1 / Math.PI) * (float)Math.Acos(ray.direction.Z) / ((float)Math.Sqrt(ray.direction.X * ray.direction.X + ray.direction.Y * ray.direction.Y) + 0.00000000001f);
            float uCood = ray.direction.X * r;
            float vCood = ray.direction.Y * r;
            int screenX = (int)((uCood + 1) * 499.5f);
            int screenY = (int)((vCood + 1) * 499.5f);

            int index = (screenX + screenY * 1000) * 3;
            return new Vector3(HDRColorBuffer[index], HDRColorBuffer[index + 1], HDRColorBuffer[index + 2]);
        }
        public void InputHandler()
        {
            var keyboard = Keyboard.GetState();

            //offset camera rotation
            if (keyboard[Key.Left])  { camOffsetDirX = .05f; keyPressed = true; }
            if (keyboard[Key.Right]) { camOffsetDirX = -.05f; keyPressed = true; }
            if (keyboard[Key.Up])    { camOffsetDirY = .05f; keyPressed = true; }
            if (keyboard[Key.Down])  { camOffsetDirY = -.05f; keyPressed = true; }

            //offset camera position
            if (keyboard[Key.W]) { camOffsetPosZ += camSpeed; keyPressed = true; }
            if (keyboard[Key.A]) { camOffsetPosX += camSpeed; keyPressed = true; }
            if (keyboard[Key.S]) { camOffsetPosZ -= camSpeed; keyPressed = true; }
            if (keyboard[Key.D]) { camOffsetPosX -= camSpeed; keyPressed = true; }

            if (keyPressed)
            {
                pixelStep = 5;
                renderReflection = false;
                renderSpecular = false;

                //calculate new camera direction
                Vector3 R = Vector3.Normalize(Vector3.Cross(camera.Direction, new Vector3(0, 1, 0))); //vector that points right 
                Vector3 U = Vector3.Cross(r, camera.Direction); //vector that points upward from screen 
                Vector3 newDir = camera.Direction + camOffsetDirX * R + camOffsetDirY * U;
                camera.Direction = Vector3.Normalize(newDir);

                //calulate new camera postion
                Vector3 oldPos = camera.Position;
                Vector3 newPos = camera.Position + camera.Direction * camOffsetPosZ + R * camOffsetPosX;
                camera.Position = newPos;
                CalculateScreenCoordinates();

                //calculate new debug screen offset
                offsetX -= (newPos.X - oldPos.X);
                offsetY += (newPos.Z - oldPos.Z);

                //reset offset values
                camOffsetDirX = 0;
                camOffsetDirY = 0;
                camOffsetPosX = 0;
                camOffsetPosZ = 0;
            }
        }

        //read information of skydome into HDRColorBuffer array
        private void ReadSkydome()
        {
            WinFileIO wf = new WinFileIO(HDRColorBuffer);
            wf.OpenForReading("../../assets/grace_probe.float");
            wf.Read(1000 * 1000 * 3 * 4);
            wf.Dispose();
        }

        //Translate x value to screen coordinates
        private int TX(float f)
        {
            f += xRange + offsetX / zoom;
            f += ((xRange * (1 - zoom)) / zoom); //translate f based on zoom factor
            f = (screen.width * f) / (xRange * 2);
            f = f * zoom;

            return (int)f;
        }

        //Translate y value to screen coordinates
        private int TY(float f)
        {
            float aspect = (float)screen.width / (float)screen.height;
            f = f * -1;
            f += yRange + offsetY / zoom;
            f += ((yRange * (1 - zoom)) / zoom); //translate f based on zoom factor
            f = (screen.height * f) / (yRange * 2);
            f = f * zoom;

            return (int)f;
        }
    }
}
