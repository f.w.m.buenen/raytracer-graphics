﻿using System;
using OpenTK;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Template
{
    class Camera
    {
        private Vector3 position, direction;
        private float screenWidth, screenDist, fov;

        public Vector3 Position { get { return position; } set { position = value; } }
        public Vector3 Direction {  get { return direction; } set { direction = value ; } }
        public float ScreenDist { get { return screenDist; } }
        public float FOV { get { return fov; } }
        public float ScreenWidth { get { return screenWidth; } }
        public float Gamma { get; }


        public Camera(Vector3 pos, Vector3 dir, float fov, float gamma = 1.8f)
        {
            this.position = pos;
            this.direction = Vector3.Normalize(dir);
            this.fov = fov;
            screenWidth = 1;
            Gamma = gamma;

            float rad = ((float)Math.PI / 180f) * (fov / 2);
            this.screenDist = screenWidth / (float)Math.Tan(rad);
        }

    }
}
