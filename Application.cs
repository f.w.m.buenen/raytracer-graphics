﻿using System;
using System.IO;
using OpenTK.Input;

namespace Template
{

    class Application
    {
        public RayTracer raytracer;

        public void Init()
        {
            raytracer = new RayTracer();
        }
        // tick: renders one frame
        public void Tick()
        {
            raytracer.InputHandler();
            raytracer.Render();
        }
    }

}
