﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Template
{
    class Plane : Primitive
    {
        Vector3 normal, position;
        float distance;

        public Vector3 Normal { get { return normal; } }
        public Vector3 Position { get { return normal; } }
        public float Distance { get { return distance; } }

        public Plane(Vector3 norm, float dist, Vector3 col, bool IsChecker, float reflectance = 0f) : base(col, IsChecker, reflectance)
        {
            normal = Vector3.Normalize(norm);
            distance = dist;
        }

        //constructor for specularity option
        public Plane(Vector3 norm, float dist, Vector3 col, float specularity, Vector3 specularColor, 
            bool IsChecker, float reflectance = 0f) : base(col, specularity, specularColor, IsChecker, reflectance)
        {
            normal = Vector3.Normalize(norm);
            distance = dist;
        }

        //returns intersection information of a ray with a sphere. returns zero if no intersection was found.
        public Intersection IntersectPlane(Ray ray)
        {
            float t = -(Vector3.Dot(normal, ray.origin) + distance) / Vector3.Dot(normal, ray.direction);

            if (t > 0 & t < ray.t)
            {
                return new Intersection(t, this, normal);
            }

            return null;
        }
    }
}
