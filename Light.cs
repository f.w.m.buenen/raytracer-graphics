﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Template
{
    class Light
    {
        Vector3 location, intensity;

        public Vector3 Location { get { return location; } }
        public Vector3 Intensity { get { return intensity; } }

        public Light(Vector3 loc, Vector3 intens)
        {
            location = loc;
            intensity = intens;
        }

        //returns theta
        public virtual float CalculateTheta(Intersection i, Vector3 L)
        {
            float theta = Vector3.Dot(i.IntersectNormal, L);
            return Math.Max(theta, 0);
        }
    }
}
